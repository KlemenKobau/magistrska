import os
from pathlib import Path
import json
import numpy as np
import pandas as pd

from existing.main import calibrate_magnetic_wifi_ibeacon_to_position, extract_wifi_rssi, extract_magnetic_strength, \
    extract_ibeacon_rssi, extract_wifi_count


def floor_mapper(floor: str):
    if len(floor) != 2:
        return np.nan

    sort = sorted(floor)  # the digit will be first

    if len(sort) != 2:
        raise RuntimeError(sort)

    digit = int(sort[0])
    if sort[1] == 'B':
        return - digit
    elif sort[1] == 'F':
        return digit - 1

    return np.nan

def initial_data_processing(site_id, floor):
    common_path = './data/{}/{}'.format(site_id, floor)
    train_data_path = common_path + '/path_data_files'
    path_filenames = list(Path(train_data_path).resolve().glob("*.txt"))
    mwi_datas = calibrate_magnetic_wifi_ibeacon_to_position(path_filenames)

    formatted = []

    for key, value in mwi_datas.items():
        out = {}
        out['magnetic'] = extract_magnetic_strength({key: value})[key].tolist()
        out['wifi'] = dict(map(lambda x: (x[0], x[1][key].tolist()), extract_wifi_rssi({key: value}).items()))
        out['beacon'] = dict(map(lambda x: (x[0], x[1][key].tolist()), extract_ibeacon_rssi({key: value}).items()))
        out['wifi_count'] = extract_wifi_count({key: value})[key]
        out['coordinates'] = key
        formatted.append(out)

    with open("{}/{}_{}.txt".format(common_path, site_id, floor), "w") as f:
        json.dump(formatted, f)


def load_mean_data(site_id, floor):
    common_path = './data/{}/{}'.format(site_id, floor)

    with open("{}/{}_{}.txt".format(common_path, site_id, floor), "r") as f:
        return json.load(f)


def unique_wifi_ids(site_id, floor):
    data = load_mean_data(site_id, floor)

    unique_ids = set()

    for point in data:
        wifi = point['wifi']

        for wifi_access_point_id in wifi.keys():
            unique_ids.add(wifi_access_point_id)

    return unique_ids


def vectorize_wifi(site, floor, save=False):
    common_path = './data/{}/{}'.format(site, floor)
    data = load_mean_data(site, floor)
    unique_wifis = unique_wifi_ids(site, floor)
    sorted_wifis = sorted(list(unique_wifis))

    table = []

    for point in data:
        wifi = point['wifi']

        row = point['coordinates']
        for sorted_wifi in sorted_wifis:
            if sorted_wifi in wifi:
                row.append(wifi[sorted_wifi][0])
            else:
                row.append(-1000)
        table.append(row)

    dataframe = pd.DataFrame(table, columns=['x', 'y'] + sorted_wifis)
    if save:
        dataframe.to_csv("{}/{}_{}_wifi.txt".format(common_path, site, floor))

    return


if __name__ == '__main__':
    dataframe = vectorize_wifi('site1', 'F1', True)
    print(dataframe)
