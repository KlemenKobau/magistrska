import pandas as pd
import numpy as np

FLOOR_PENALTY = 15


def error_score(ground_truth: pd.DataFrame, result: pd.DataFrame):
    # the data frames contain rows with (x,y,floor), describing a waypoint

    num_rows = len(ground_truth)

    absolute_position_diff: pd.DataFrame = ground_truth.loc[:, 1:3] - result.loc[:, 1:3]
    error_pos = np.linalg.norm(absolute_position_diff)
    error_floors = np.sum(np.abs(ground_truth.loc[:, 3] - result.loc[:, 3])) * FLOOR_PENALTY

    return error_pos / num_rows + error_floors / num_rows

