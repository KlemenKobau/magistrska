import pandas as pd
from sklearn.neighbors import KNeighborsRegressor

if __name__ == '__main__':
    dataframe = pd.read_csv('data/site1/F1/site1_F1_wifi.txt')
    train = dataframe.iloc[:-100]
    test = dataframe.iloc[-100:]

    y = train.iloc[:, 1:3]
    X = train.iloc[:, 3:]
    regressor = KNeighborsRegressor(n_neighbors=5)
    regressor.fit(X, y)
    predictions = regressor.predict(test.iloc[:, 3:])

    print(predictions)
